package org.eclipse.banking.management;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Deposit implements Transaction
{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	Sqlcon c=new Sqlcon();
	PreparedStatement ps;
	int choi;
	String mode=null;
	String date;
	public void transop(int cuid,long accno,float amount,String type)
	{int tid=0;
	type="Deposit";
	try {
		ps = c.con.prepareStatement("insert into transactions(custid,accno,transtype,amount,modes) values(?,?,?,?,?)");
	System.out.println("Deposit in 1.Cash 2.Cheque");
	choi=Integer.parseInt(br.readLine());
	if(choi==1)
	{mode="Cash";}
	else { mode="Cheque";}
	ps.setInt(1, cuid);
	ps.setLong(2, accno);
	ps.setString(3, type);
	ps.setFloat(4, amount);
	ps.setString(5, mode);
	ps.executeUpdate();
	ps=c.con.prepareStatement("select transid,datetim from transactions where accno=?");
	ps.setLong(1, accno);
	ResultSet rs= ps.executeQuery();
	while (rs.next()) {
	tid=rs.getInt(1);
	date=rs.getString(2);}
	System.out.println("Transaction no. "+tid+" completed succesfully at "+date);
	Account t1=new Account();
	t1.transop(cuid, accno, amount, type);
	}catch(Exception e)
	{System.out.println(e);}
	}
}
package org.eclipse.banking.management;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class NewAccount {
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	Sqlcon c=new Sqlcon();
	PreparedStatement ps;
	String type;
	long accno;
	float bal;
	NewAccount(int cusid)
	{
	System.out.println("Please deposit Starting cash for Account:");
	try {
	ps = c.con.prepareStatement("insert into accounts(custid,actype,accbal) values(?,?,?);");
	System.out.println("Choose 1.Savings Account 2.Recurring Account");
	int choice=Integer.parseInt(br.readLine());
	if(choice==1)
	{type="Savings";
	}else type="Recurring Account";
	System.out.println("Enter Inital Deposit(Minimum 1000):");
	bal=Float.parseFloat(br.readLine());
	while(bal<1000)
	{System.out.println("Please enter amount greater than 1000");
	bal=Float.parseFloat(br.readLine());}
	ps.setInt(1, cusid);
	ps.setString(2, type);
	ps.setDouble(3, bal);
	ps.executeUpdate();
	Deposit t1=new Deposit();
	ps = c.con.prepareStatement("select accno from accounts where custid=?");
	ps.setInt(1, cusid);
	ResultSet rs= ps.executeQuery();
	while (rs.next()) {
	accno=rs.getLong(1);}
	System.out.println("Customer "+cusid+" has been allocated "+type+" type account with Account No: "+accno);
	t1.transop(cusid,accno,bal,type);
	}catch(Exception e)
	{System.out.println(e);}
	}
}
